import pytest
from license_seeker.apis.converters.github_converter import GithubConverter

def test_convert():
    """
    The for valid url.
    """
    url = "https://github.com/torvalds/linux"
    api_url = "https://api.github.com/repos/torvalds/linux"

    url = GithubConverter.convert(url)

    assert api_url == url

def test_convert_fail():
    """
    The for invalid url.
    """
    url = "https://pypi.org/project/rich/"
    url = GithubConverter.convert(url)

    assert url is None

def test_name():
    name = "api.github.com"
    assert name == GithubConverter.get_name()

