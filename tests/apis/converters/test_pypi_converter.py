import pytest
from license_seeker.apis.converters.pypi_converter import PypiConverter

def test_convert():
    """
    The for valid url.
    """
    url = "https://pypi.org/project/rich/"
    api_url = "https://pypi.org/pypi/rich/json"

    url = PypiConverter.convert(url)

    assert api_url == url

def test_convert_without_last_slash():
    """
    The for valid url that do not ends with "/".
    """
    url = "https://pypi.org/project/rich"
    api_url = "https://pypi.org/pypi/rich/json"

    url = PypiConverter.convert(url)

    assert api_url == url

def test_convert_fail():
    """
    The for invalid url.
    """
    url = "https://github.com/torvalds/linux"
    url = PypiConverter.convert(url)

    assert url is None

def test_name():
    name = "pypi.org"
    assert name == PypiConverter.get_name()

